﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructures
{
    class HashMap
    {
        private String[] data;

        public void add(String key, String value)
        {
            int index = hash(key);
            data[index] = value;
        }
        public void remove(String key) { }
        public String get(String key) { }

        protected int hash(String key)
        {
            int hash = 0;
            foreach (Char c in key)
            {
                hash += c;
            }

            return hash;
        }

    }
}
