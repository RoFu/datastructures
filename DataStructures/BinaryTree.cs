﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructures
{
    class BinaryTree<T> where T : IComparable
    {
        private Node root;

        private class Node
        {
            public T data;
            public Node left;
            public Node right;
        }

        public void add(T value)
        {
            Node temp = root;

            if (root == null)
            {
                root = new Node();
                root.data = value;
            }
            else
            {
                while (temp != null)
                {
                    if (value.CompareTo(temp.data) > 0)
                    {
                        temp = temp.right;
                    }
                    else if (value.CompareTo(temp.data) < 0)
                    {
                        temp = temp.left;
                    }
                    else
                    {
                        // clear off
                    }
                }

                temp = new Node();
                temp.data = value;
            }
        }
        public void remove() { }
        public void removeAll() { }
        public void get() { }
        public void print() { }
    }
}
