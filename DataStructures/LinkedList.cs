﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructures
{
    class LinkedList<T>
    {
        private Node head;

        private class Node
        {
            public T data;
            public Node next;
        }

        public void add(T value)
        {
            Node node = new Node();
            node.data = value;
            node.next = head;
            head = node;
        }

        public void print()
        {
            Node temp = head;
            while (temp != null)
            {
                Console.WriteLine(temp.data);
                temp = temp.next;
            }
        }

        public T get(int index)
        {
            return default(T);
        }

        public void removeAll(T val)
        {
        }

        public void remove(T val)
        {
        }

        private void remove(T val, Boolean all)
        {

        }
    }
}
