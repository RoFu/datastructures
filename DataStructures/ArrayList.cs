﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataStructures
{
    class ArrayList<T>
    {
        private T[] theArray;
        private int index = 0;

        public ArrayList(int size)
        {
            theArray = new T[size];
        }

        public void add(T value)
        {
            theArray[index] = value;
            index++;
            if (index > theArray.Length) {
                Resize(theArray.Length * 2);
            }
        }

        public T get(int index)
        {
            return theArray[index];
        }

        public void removeAll(T val){
            remove(val, false);
        }

        public void remove(T val) {
            remove(val, true);
        }

        private void remove(T val, Boolean all) {
            for (int i = 0; i < theArray.Length; i++)
            {
                if (theArray[i].Equals(val))
                {
                    theArray[i] = default(T);
                    if (all) return;
                }
            }
        }


        private void Resize(int newSize) {
            Array.Resize(ref theArray, newSize);
        }

        private void Compact() { }
    }
}
